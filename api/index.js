const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
require('dotenv').config({ path: 'variables.env'})
const mongoose = require('mongoose');
const createServer = require('./createServer');

const db = mongoose.connect(process.env.DB, { 
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
} ).catch((err) => {
  console.log("Not Connected to Database ERROR! ", err);
});;

const server = createServer();

// middleware to handle cookies in every page, like JWT
server.express.use(cookieParser());

// decode JWT from cookie
server.express.use((req, res, next) => {
  const { token } = req.cookies;
  if(token){
    const {userId} = jwt.verify(token, process.env.APP_SECRET);
    req.userId = userId ;
  }
  next();
});

// Use these to avoid PayloadTooLargeError when passing 64base image
server.express.use(bodyParser.json({limit: '10mb', extended: true}))
server.express.use(bodyParser.urlencoded({limit: '10mb', extended: true}))

server.start({
    cors: {
      credentials: true,
      origin: process.env.FRONTEND_URL
    },
    playground: process.env.NODE_ENV === 'development' ? '/' : false,
  }, deets => {
    console.log(`server is now running on port http://localhost:${deets.port}`)
});

  
