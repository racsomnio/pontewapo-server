const typeDefs = `
  type User {
    _id: ID
    username : String!
    password : String!
    email : String!
    createdAt : String
    isGood : [Food]
    isOk : [Food]
    isBad : [Food]
    resetToken: String
    resetTokenExpiry: Float
  }

  type Food {
    _id: ID
    name : String!
    price: Float!
    typeOfBusiness: String!
    country: String!
    isGood: Int
    isBad: Int
    isOk: Int
    pics: [Cloudinary]
    comments: [String]
    createdDate: String
    username: String
    location: Location
  }

  type Store {
    _id: ID
    rights: [String]
    name: String!
    typeOfBusiness: [String]!
    location: Location
    email: String!
    phone: [Phone]
    wa_orders: Boolean
    createdDate : String
    cover_img: [Cloudinary]
    profile_img: [Cloudinary]
    message: String
    speciality: [String]
    hours: [Hours]
    social: Social
    food_type: Country
    delivery: Boolean
    takeaway: Boolean
    only_cash: Boolean
    card: Boolean
    open_tv: Boolean
    cable: Boolean
    ppv: Boolean
    speak_english: Boolean
    english_menu: Boolean
    vegan_option: Boolean
    vegetarian_option: Boolean
    no_gluten_option: Boolean
    no_shellfish_option: Boolean
    reservations: Boolean
    live_music: Boolean
    dancing: Boolean
    rooftop: Boolean
    terrace: Boolean
    garden: Boolean
    smoking_area: Boolean
    kids_menu: Boolean
    kids_area: Boolean
    wifi: Boolean
    private_room: Boolean
    parking: Boolean
    valet_parking: Boolean
    pet_friendly: Boolean
    catering: Boolean
    gay: Boolean
    has_breakfast: Boolean
    has_lunch_special: Boolean
    has_healthy_food: Boolean
    has_alcohol: Boolean
    has_billiard: Boolean
    has_pool: Boolean
    has_foosball: Boolean
    has_table_games: Boolean
    has_karaoke: Boolean
    big_groups: Boolean
    etiquette: String
    menus: [Menu]
    resetToken: String
    resetTokenExpiry: Float
    slug: String
    stripeCustomerId: String
    full_menu: FullMenu
    stored_pics: [String]
  }

  type Menu {
    _id: ID
    created_at: String
    expires_at: String
    meal: String
    repeat: [Int]
    html_menu: String
    price: String
    hours: String
    promotion: String
    menu_image: [Cloudinary]
    location: Location
    owner: Store
    distance: Float
  }

    type Hours {
      from: String
      to: String
      open_hr: String
      open_min: String
      open_ampm: String
      close_hr: String
      close_min: String
      close_ampm: String
      _id: String
    }

    input HoursInput {
      from: String
      to: String
      open_hr: String
      open_min: String
      open_ampm: String
      close_hr: String
      close_min: String
      close_ampm: String
    }

    type Social {
        facebook: String
        instagram: String
        twitter: String
        pinterest: String
        website: String
        _id: String
    }

    input SocialInput {
        facebook: String
        instagram: String
        twitter: String
        pinterest: String
        website: String
    }

    type Country {
        code: String
        label: String
        phone: String
        type: String
    }

    input CountryInput {
        code: String
        label: String
        phone: String
        type: String
    }

    type Location {
        address: String
        coordinates: [Float]
        placeId: String
        _id: String
    }

    input LocationInput {
        address: String
        coordinates: [Float]
        placeId: String
    }

    type Phone {
        number: String!
        whatsapp: Boolean
        _id: String
    }

    input PhoneInput {
        number: String
        whatsapp: Boolean
    }
  
  type FullMenu {
    _id: ID
    menu: [MenuSections]
    customized: PickColors
    created_at: String
    owner: Store
  }
    type MenuSections {
      _id: String
      section_title: String
      section_description: String
      section_footer: String
      section_image: String
      food_items: [FoodItems]
      active: Boolean 
    }

    input MenuSectionsInput {
      section_title: String
      section_description: String
      section_footer: String
      section_image: String
      food_items: [FoodItemsInput] 
      active: Boolean 
    }

    type FoodItems {
      _id: String
      food_name: String
      food_image: String
      food_price: [PriceItems]
      food_description: String
      food_tags: [String]
      active: Boolean
    }

    input FoodItemsInput {
      food_name: String
      food_image: String
      food_price: [PriceItemsInput]
      food_description: String
      food_tags: [String]
      active: Boolean 
    }

    type PriceItems {
      _id: String
      price_value: String
      price_description: String
    }

    input PriceItemsInput {
      price_value: String
      price_description: String
    }

    type PickColors {
      _id: String
      page_background_color: String
      title_background_color: String
      title_text_color: String
      items_background_color: String
      items_text_color: String
    }

    input PickColorsInput {
      page_background_color: String
      title_background_color: String
      title_text_color: String
      items_background_color: String
      items_text_color: String
    }

  type Token {
    token: String!
  }

  type Message {
    message: String!
  }

  type Cloudinary {
    secure_url: String
    public_id: String
    format: String
  }

  input CloudinaryInput {
    secure_url: String
    public_id: String
    format: String
  }

  union Owner = User | Store

  enum AllowedMeals {
    breakfast
    lunch
    dinner
    drinks
    dessert
    snacks
    beverages
  }

  type Query{
    getCurrentUser(_id: ID): Owner
    getMyStore(_id:ID): Store
    getStore(_id: ID!): Store
    getStoreBySlug(slug: String): Store
    getAllStores: [Store]
    getMenus( coordinates: String, meal: AllowedMeals ): [Menu]
    getMenu( _id: ID! ): Menu
    getMyMenus( _id: ID ): [Menu]
  }

  type Mutation{
    signupUser(username: String!, email: String!, password: String! ): Token
    signinUser(email: String!, password: String!): Owner
    createStore(
        name: String!, 
        email: String!, 
        password: String!,
        location: LocationInput,
        phone: [PhoneInput],
        typeOfBusiness: [String]
    ): Store!
    requestReset(email: String!): Message
    resetPassword(resetToken: String!, password: String!): Owner!

    updateStoreImgs(_id: ID, flag: String, img64base: String): Store
    updateStoreBooleans(_id: ID, flag: String, res: Boolean): Store
    updateStorePhones(_id: ID, phones:[PhoneInput]): Store
    updateStoreFoodType(_id:ID, food_type:CountryInput ): Store
    updateStoreSpeciality(_id:ID, speciality:[String] ): Store
    updateStoreHours(_id:ID, hours:[HoursInput] ): Store

    createMenu(meal: String, repeat: [Int], html_menu: String, menu_image: [CloudinaryInput], price: String, hours: String, promotion: String, location: LocationInput, owner: String): Menu
    updateMenu(_id: ID!, meal: String, repeat: [Int], html_menu: String, menu_image: [CloudinaryInput], price: String, hours: String, promotion: String): Menu
    deleteMenu(menuId: ID!, ownerId: ID!): Menu

    createFullMenu(menu: [MenuSectionsInput], ownerId: String): FullMenu
    updateFullMenu(fullMenuId: String, menu: [MenuSectionsInput], ownerId: String): FullMenu
    updateCustomized(fullMenuId: String, customized: PickColorsInput): FullMenu

    uploadImage(img64base: String): Cloudinary
    storePic(img64base: String, _id: ID): Message

    createStripeSession( plan: String! ): Message
    createStripeSubscription( stripeSessionId: String!): Store

    signout: Message
  }
`
module.exports = typeDefs;