const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fullMenuSchema = new Schema({
    created_at:{
        type: Date,
        default: Date.now,
    },
    menu: [{
        section_title: {
            type: String
        },
        section_description: {
            type: String
        },
        section_footer: {
            type: String
        },
        section_image: {
            type: String
        },
        food_items: [{
            food_name: {
                type: String
            },
            food_image: {
                type: String
            },
            food_price: [{
                price_value: {
                    type: String
                },
                price_description: {
                    type: String
                }
            }],
            food_description: {
                type: String
            },
            food_tags: {
                type: [String],
                // enum: ["spicy", "green", "healthy", "popular"]
                enum: ["Picoso", "Vegetariano", "Popular"]
            },
            active: {
                type: Boolean,
                default: true
            },
        }],
        active: {
            type: Boolean,
            default: true
        },
    }],
    customized: {
        page_background_color: {
            type: String
        },
        title_background_color: {
            type: String
        },
        title_text_color: {
            type: String
        },
        items_background_color: {
            type: String
        },
        items_text_color: {
            type: String
        },
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'Store',
    }
})

mongoose.set('useCreateIndex', true); // needed to avoid deprecation warning
module.exports = mongoose.model('FullMenu', fullMenuSchema)