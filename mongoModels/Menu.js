const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const menuSchema = new Schema({
    created_at:{
        type: Date,
        default: Date.now,
    },

    expires_at:{
        type: Date,
        default: Date.now() + 6.04e+8 // 6.04e+8 = 1week in ms, (6.04e+8 * 2) = 2 weeks
    },

    meal: {
        type: String
    },

    repeat: [{
        type: String
    }],

    html_menu: {
        type: String
    },

    price: {
        type: String
    },

    hours: {
        type: String
    },

    promotion: {
        type: String
    },

    menu_image: [{
        secure_url: {
            type: String
        },
        public_id: {
            type: String
        },
        format: {
            type: String
        },
    }],

    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [{
            type: Number
        }],
        address: {
            type: String        
        }
    },

    owner: {
        type: Schema.Types.ObjectId,
        ref: 'Store',
    }
   
});

mongoose.set('useCreateIndex', true); // needed to avoid deprecation warning
menuSchema.index({
    location: '2dsphere' // needed for geoNear
})

module.exports = mongoose.model('Menu', menuSchema)