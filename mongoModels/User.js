const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const userSchema = new Schema({
    username : {
        type : String,
        required: true,
    },
    email:{
        type: String,
        required: true,
        unique: true,
    },
    password:{
        type: String,
        required: true,
    },
    createdAt:{
        type: Date,
        default: Date.now,
    },
    isGood:{
        type:[Schema.Types.ObjectId],
        ref: 'Food',
    },
    isOk:{
        type:[Schema.Types.ObjectId],
        ref: 'Food',
    },
    isBad:{
        type:[Schema.Types.ObjectId],
        ref: 'Food',
    },
    resetToken: {
        type: String
    },
    resetTokenExpiry: {
        type: Date
    }
});

userSchema.pre('save', function(next){
    if(!this.isModified('password')){
        return next();
    } 
    bcrypt.genSalt(10, (err, salt) => {
        if(err) return next(err);
        bcrypt.hash(this.password, salt, (err, hash) => {
            if(err) return next(err);
            this.password = hash;
            next();
        })
    })
});

module.exports = mongoose.model('User', userSchema);
