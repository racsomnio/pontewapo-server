const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../mongoModels/User');
const Store = require('../mongoModels/Store');
const Menu = require('../mongoModels/Menu');
const FullMenu = require('../mongoModels/FullMenu');
const { randomBytes } = require('crypto'); // Buil-in node module
const { promisify } = require('util'); // Buil-in node module
const { transport, makeANiceEmail } = require('../mail');
const cloudinary = require('cloudinary').v2;
const stripe = require('../stripe');

const Mutations = {
    signupUser: async (root, {username, email, password}, context) => {
        let isAlready;
        isAlready = await User.findOne({email});
        if(!isAlready) isAlready = await Store.findOne({email});
        if(isAlready) throw new Error ('Éste compañero ya existe en ésta corporación')

        const newUser = await new User({
            username,
            email,
            password,
        }).save();

        // generate the JWT Token
        const token = jwt.sign({ userId: newUser._id }, process.env.APP_SECRET);

        // Set cookie with the token        
        context.response.cookie('token', token, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 365, // 1 year cookie
            sameSite: 'none',
            // secure: process.env.NODE_ENV == 'development' ? '' : true,
            secure: true
        });

        return {
            token
        }
    },

    signinUser: async (root, {email, password}, context) => {
        let user;
        user = await Store.findOne({email});
        if(!user) user = await User.findOne({email});
        if(!user) throw new Error("Este correo no existe");

        const isValidPassword = await bcrypt.compare(password, user.password);
        if(!isValidPassword) throw new Error("Contraseña incorrecta");

        // generate the JWT Token
        const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
        // Set the cookie with the token
        context.response.cookie('token', token, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 365,
            sameSite: 'none',
            // secure: process.env.NODE_ENV == 'development' ? false : true,
            secure: true
        });
        return user
    },

    requestReset: async (root, {email}, context) => {
        // 1. Check if this is a real user
        let user;
        user = await User.findOne({email});
        if(!user) user = await Store.findOne({email});
        if(!user) throw new Error("Este correo no existe");

        // 2. Set a reset token and expiry on that user
        const randomBytesPromiseified = promisify(randomBytes);
        const resetToken = (await randomBytesPromiseified(20)).toString('hex');
        const resetTokenExpiry = Date.now() + 3600000; // 1 hour from now
        user.resetToken = resetToken;
        user.resetTokenExpiry = resetTokenExpiry;
        user.save();

        // 3. Email them that reset token
        const mailRes = await transport.sendMail({
            from: 'holacomidagodin@gmail.com',
            to: user.email,
            subject: 'Actualiza tu contraseña',
            html: makeANiceEmail(
                `<div>
                    Para modificar tu contraseña visita el siguiente enlace
                    <br/>
                    <a href="${process.env.FRONTEND_URL}/cambiar-contrasena?reset=${resetToken}">Actualizar Contraseña</a>
                    <br/>
                    <small>Este enlace expirará en una hora.</small>
                    <br/>
                    <br/>
                </div>`
            ),
        });

        return { message: 'Thanks!' };
    },

    resetPassword: async (root, {resetToken, password}, context) => {
        const hashPass = await bcrypt.hash(password, 10);

        // check if its a legit reset token and Check if its expired
        // we query users in order to query for nodes that are not unique, like resetToken, instead of quering for ID or email.
        let user;
        user = await User.findOneAndUpdate({
            resetToken,
            resetTokenExpiry: { $gte: Date.now() - 3600000 }
            },
            {
                password: hashPass,
                resetToken: null,
                resetTokenExpiry: null,
            }
        );
        if (!user) {
            user = await Store.findOneAndUpdate({
                resetToken,
                resetTokenExpiry: { $gte: Date.now() - 3600000 }
                },
                {
                    password: hashPass,
                    resetToken: null,
                    resetTokenExpiry: null,
                }
            ); 
        }

        if (!user) {
            throw new Error('Tu tiempo para actualizar tu correo ha expirado.');
        }
        
        return user;
    },

    uploadImage: async (root, {img64base}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const res = await cloudinary.uploader.upload(
            img64base,
            {upload_preset: process.env.CLOUDINARY_PRESET},
            function (err, image) {
                if (err) { console.warn(err); }
                // console.log("* " + image.public_id);
                // console.log("* " + image.url);
            }
        );

        return { 
            secure_url: res.secure_url,
            public_id: res.public_id,
            format: res.format
        }
    },

    storePic: async (root, {img64base, _id}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const storeId = _id || userId;
        if(_id) {
            const isEditor = await Store.findOne({ _id:userId, rights: "EDITOR"})
            if(!isEditor) throw new Error('Sólo personal autorizadow');
        }
        const res = await cloudinary.uploader.upload(
            img64base,
            {upload_preset: process.env.CLOUDINARY_PRESET},
            function (err, image) {
                if (err) { console.warn(err); }
                // console.log("* " + image.public_id);
                // console.log("* " + image.url);
            }
        );

        const store = await Store.findByIdAndUpdate(storeId, { $addToSet: { stored_pics: [ res.secure_url ] } } );

        return { 
            message: res.secure_url
        }
    },

    createStore: async (root, { name, typeOfBusiness, location, email, phone, password }, context) => {
        let isAlready;
        isAlready = await User.findOne({email});
        if(!isAlready) isAlready = await Store.findOne({$or: [ {email}, { "phone.number": phone[0].number }]});
        if(isAlready) throw new Error ("Este teléfono ó email ya existe.")

        const newStore = await new Store({
            name,
            typeOfBusiness,
            location,
            email,
            phone,
            password,
        }).save();

        // generate the JWT Token
        const token = jwt.sign({ userId: newStore._id }, process.env.APP_SECRET);

        // Set cookie with the token        
        context.response.cookie('token', token, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 365, // 1 year cookie
            sameSite: 'none',
            // secure: process.env.NODE_ENV == 'development' ? false : true,
            secure: true
        });

        // Email them that reset token
        const mailRes = await transport.sendMail({
            from: 'holacomidagodin@gmail.com',
            to: email,
            subject: 'Bienvenido a Comida Godín',
            html: makeANiceEmail(`
                <p>
                Te has registrado con éxito 
                </p>
            `),
        });

        return newStore
    },

    updateStoreImgs: async (root, {_id, flag, img64base}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const store = await Store.findOne({ _id: userId });
        const res = await cloudinary.uploader.upload(
            img64base,
            {upload_preset: process.env.CLOUDINARY_PRESET},
            function (err, image) {
                if (err) { console.warn(err); }
                // console.log("* " + image.public_id);
                // console.log("* " + image.url);
            }
        );
        
        store[flag].unshift({
            secure_url: res.secure_url,
            public_id: res.public_id,
            format: res.format
        });

        store.save();

        return store;
    },

    updateStoreBooleans: async (root, { _id, flag, res }, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }
        
        const store = await Store.findOneAndUpdate({ _id: userId }, {$set: { [flag]: res }});
        return store;
    },

    updateStorePhones: async (root, { _id, phones }, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const store = await Store.findOneAndUpdate({ _id: userId }, {$set: { phone: phones }});
        return store;
    },

    updateStoreFoodType: async (root, {food_type}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const store = await Store.findOneAndUpdate({ _id: userId }, {$set: { food_type }});
        return store;
    },

    updateStoreSpeciality: async (root, {speciality}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const store = await Store.findOneAndUpdate({ _id: userId }, {$set: { speciality }});
        return store;
    },

    updateStoreHours: async (root, {hours}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const store = await Store.findOneAndUpdate({ _id: userId }, {$set: { hours }});
        return store;
    },

    createMenu: async (root, args, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const menu = await new Menu(args).save();
        const store = await Store.findById(userId);
        await store.menus.push(menu);
        await store.save();

        return menu;
    },

    createFullMenu: async (root, {menu, ownerId}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }
        const storeId = ownerId || userId;

        const full_menu = await new FullMenu({
            menu,
            owner: storeId
        }).save();
        const store = await Store.findByIdAndUpdate(storeId, { full_menu });

        return full_menu;
    },

    updateFullMenu: async (root, {fullMenuId, menu, ownerId}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }
        const storeId = ownerId || userId;
        const full_menu = await FullMenu.findOne({ _id: fullMenuId});
        const store = await Store.findOne({ _id: storeId });
        const can_edit = store.full_menu == fullMenuId || store.rights.includes('EDITOR')

        if(!can_edit){
            return null;
        }

        full_menu.menu = menu;
        await full_menu.save()
        return full_menu;
    },

    updateCustomized: async (root, {fullMenuId, customized}, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const full_menu = await FullMenu.findOne({ _id: fullMenuId});
        const store = await Store.findOne({ _id: userId });
        const can_edit = store.full_menu == fullMenuId || store.rights.includes('EDITOR')

        if(!can_edit){
            return null;
        }

        full_menu.customized = customized;
        await full_menu.save()
        return full_menu;
    },

    updateMenu: async (root, args, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }

        const menu = await Menu.findOne({ _id: args._id }).populate({
            path: 'owner',
            model: 'Store',
        });

        // console.log("owner: ", menu.owner._id)
        // console.log("userId: ", userId)

        if(menu.owner._id != userId) throw new Error('Estas en la oficina equivocada!')

        menu.meal = args.meal;
        menu.repeat = args.repeat;
        menu.html_menu = args.html_menu;
        menu.menu_image = args.menu_image

        await menu.save();

        return menu;
    },

    deleteMenu: async (root, { menuId, ownerId }, context) => {
        const { userId } = context.request;
        // Check if there is a user ID
        if(!userId){
            return null;
        }
        
        const menu = await Menu.findOneAndRemove({ _id: menuId });
        await Store.findOneAndUpdate({ _id: ownerId }, {$pull: { menus: menuId }})
        return menu;
    },

    createStripeSession: async (parent, {plan}, context) => {
        // Query the current user and make sure they are signed in
        const { userId } = context.request;
        if(!userId) return { message: "" };

        const user = await Store.findOne(
            { _id: userId },
            {
                name: 1,
                email: 1,
            }
        );

        const priceFromPlan = process.env[plan];
        const customer_session = await stripe.checkout.sessions.create({
            customer_email: user.email,
            billing_address_collection: 'required',
            payment_method_types: ['card'],
            line_items: [{
              price: priceFromPlan,
              quantity: 1,
            }],
            mode: 'subscription',
            success_url: process.env.FRONTEND_URL + '/negocios/pagos?session_id={CHECKOUT_SESSION_ID}',
            cancel_url: process.env.FRONTEND_URL + '/negocios/ver-planes',
        });

        return { message: customer_session.id };

    },

    createStripeSubscription: async (parent, { stripeSessionId }, context) => {
        // Query the current user and make sure they are signed in
        const { userId } = context.request;
        if (!userId) throw new Error('Por favor ingresa ó crea una cuenta.');

        const user = await Store.findOne(
            { _id: userId },
            {
                email: 1,
                rights: 1
            }
        );

        const getSession = await stripe.checkout.sessions.retrieve(stripeSessionId, {
            expand: ['line_items'],
        })

        const { customer, customer_email, subscription, line_items } = getSession;
        const { description, price: { id: priceId, product: productId} } = line_items.data[0];
        const set_rights = description === 'PLAN LIGERO' ? 'PLAN_LITE' : 'PLAN_PREMIUM'        

        if(user.email === customer_email) {
            user.rights = set_rights;
            user.stripeCustomerId = customer;
            user.subscriptionId = subscription;
            user.productId = productId;
            user.priceId = priceId;
            
            await user.save();
        }

        return user;
    },

    signout: async (parent, args, context) => {
        context.response.clearCookie('token');
        return { message: 'Regresa Pronto!' };
    },
};

module.exports = Mutations;